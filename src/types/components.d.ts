import CpIcon from '@/components/cp-icon.vue'
import CpNavBar from '@/components/cp-nav-bar.vue'
import CpRadioBtn from '@/components/cp-radio-btn.vue'

declare module 'vue' {
  export interface GlobalComponents {
    CpIcon: typeof CpIcon
    CpNavBar: typeof CpNavBar
    CpRadioBtn: typeof CpRadioBtn
  }
}
